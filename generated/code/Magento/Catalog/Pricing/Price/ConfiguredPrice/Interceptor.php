<?php
namespace Magento\Catalog\Pricing\Price\ConfiguredPrice;

/**
 * Interceptor class for @see \Magento\Catalog\Pricing\Price\ConfiguredPrice
 */
class Interceptor extends \Magento\Catalog\Pricing\Price\ConfiguredPrice implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Model\Product $saleableItem, $quantity, \Magento\Framework\Pricing\Adjustment\CalculatorInterface $calculator, \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, ?\Magento\Catalog\Model\Product\Configuration\Item\ItemInterface $item = null, ?\Magento\Catalog\Pricing\Price\ConfiguredOptions $configuredOptions = null)
    {
        $this->___init();
        parent::__construct($saleableItem, $quantity, $calculator, $priceCurrency, $item, $configuredOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function setItem(\Magento\Catalog\Model\Product\Configuration\Item\ItemInterface $item)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setItem');
        if (!$pluginInfo) {
            return parent::setItem($item);
        } else {
            return $this->___callPlugins('setItem', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getValue');
        if (!$pluginInfo) {
            return parent::getValue();
        } else {
            return $this->___callPlugins('getValue', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMinimalPrice()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMinimalPrice');
        if (!$pluginInfo) {
            return parent::getMinimalPrice();
        } else {
            return $this->___callPlugins('getMinimalPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMaximalPrice()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMaximalPrice');
        if (!$pluginInfo) {
            return parent::getMaximalPrice();
        } else {
            return $this->___callPlugins('getMaximalPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAmount()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAmount');
        if (!$pluginInfo) {
            return parent::getAmount();
        } else {
            return $this->___callPlugins('getAmount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomAmount($amount = null, $exclude = null, $context = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomAmount');
        if (!$pluginInfo) {
            return parent::getCustomAmount($amount, $exclude, $context);
        } else {
            return $this->___callPlugins('getCustomAmount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPriceCode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPriceCode');
        if (!$pluginInfo) {
            return parent::getPriceCode();
        } else {
            return $this->___callPlugins('getPriceCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProduct');
        if (!$pluginInfo) {
            return parent::getProduct();
        } else {
            return $this->___callPlugins('getProduct', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getQuantity()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getQuantity');
        if (!$pluginInfo) {
            return parent::getQuantity();
        } else {
            return $this->___callPlugins('getQuantity', func_get_args(), $pluginInfo);
        }
    }
}
