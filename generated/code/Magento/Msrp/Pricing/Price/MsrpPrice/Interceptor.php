<?php
namespace Magento\Msrp\Pricing\Price\MsrpPrice;

/**
 * Interceptor class for @see \Magento\Msrp\Pricing\Price\MsrpPrice
 */
class Interceptor extends \Magento\Msrp\Pricing\Price\MsrpPrice implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Catalog\Model\Product $saleableItem, $quantity, \Magento\Framework\Pricing\Adjustment\CalculatorInterface $calculator, \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency, \Magento\Msrp\Helper\Data $msrpData, \Magento\Msrp\Model\Config $config)
    {
        $this->___init();
        parent::__construct($saleableItem, $quantity, $calculator, $priceCurrency, $msrpData, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function isShowPriceOnGesture()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isShowPriceOnGesture');
        if (!$pluginInfo) {
            return parent::isShowPriceOnGesture();
        } else {
            return $this->___callPlugins('isShowPriceOnGesture', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMsrpPriceMessage()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMsrpPriceMessage');
        if (!$pluginInfo) {
            return parent::getMsrpPriceMessage();
        } else {
            return $this->___callPlugins('getMsrpPriceMessage', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isMsrpEnabled()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isMsrpEnabled');
        if (!$pluginInfo) {
            return parent::isMsrpEnabled();
        } else {
            return $this->___callPlugins('isMsrpEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function canApplyMsrp(\Magento\Catalog\Model\Product $product)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'canApplyMsrp');
        if (!$pluginInfo) {
            return parent::canApplyMsrp($product);
        } else {
            return $this->___callPlugins('canApplyMsrp', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isMinimalPriceLessMsrp(\Magento\Catalog\Model\Product $product)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isMinimalPriceLessMsrp');
        if (!$pluginInfo) {
            return parent::isMinimalPriceLessMsrp($product);
        } else {
            return $this->___callPlugins('isMinimalPriceLessMsrp', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getValue');
        if (!$pluginInfo) {
            return parent::getValue();
        } else {
            return $this->___callPlugins('getValue', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMinimalPrice()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMinimalPrice');
        if (!$pluginInfo) {
            return parent::getMinimalPrice();
        } else {
            return $this->___callPlugins('getMinimalPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMaximalPrice()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMaximalPrice');
        if (!$pluginInfo) {
            return parent::getMaximalPrice();
        } else {
            return $this->___callPlugins('getMaximalPrice', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAmount()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAmount');
        if (!$pluginInfo) {
            return parent::getAmount();
        } else {
            return $this->___callPlugins('getAmount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomAmount($amount = null, $exclude = null, $context = [])
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCustomAmount');
        if (!$pluginInfo) {
            return parent::getCustomAmount($amount, $exclude, $context);
        } else {
            return $this->___callPlugins('getCustomAmount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getPriceCode()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getPriceCode');
        if (!$pluginInfo) {
            return parent::getPriceCode();
        } else {
            return $this->___callPlugins('getPriceCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getProduct');
        if (!$pluginInfo) {
            return parent::getProduct();
        } else {
            return $this->___callPlugins('getProduct', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getQuantity()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getQuantity');
        if (!$pluginInfo) {
            return parent::getQuantity();
        } else {
            return $this->___callPlugins('getQuantity', func_get_args(), $pluginInfo);
        }
    }
}
